######
Design
######

*****
Tasks
*****
.. _tasks:

********
Commands
********
.. _commands:

******************
Configuration file
******************
.. _configuration:

**************
Graph creation
**************

When a command is instanciated, the ``setup_task()`` method is called and a dependancy graph is created and will be used to execute the tasks in the right order.
