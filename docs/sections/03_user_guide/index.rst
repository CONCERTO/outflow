User guide
=====================================

.. toctree::
   :maxdepth: 2
   :caption: User guide:

   How to install Outflow<install>
   Tasks<tasks>
   Workflows<workflows>
   Commands<commands>
   Database<database>
   Backend<backend>
   Migrations<migrations>
   Outflow settings<settings>
   Outflow configuration<configuration>
   Logging<logging>
   Dashboard<dashboard>
   Design Patterns<design_patterns>
   Example Workflows<example_workflows>
   Library<library/index>
