# Logging

Logger configuration is integrated into outflow to facilitate logging from the different plugins. By default a simple configuration and formatter is loaded, you can override them in your config.yml file.

## Logging from tasks

The logger object in the logging module of outflow core is already set up with all the registered plugins:

```python
#inside a tasks.py file
from outflow.core.logging import logger

@as_task
def MyTask():
    logger.info("Hello from MyTask")
```

result :

`2021-09-30 14:17:43,459 - my_project.my_plugin - tasks.py:15 - INFO - Hello from MyTask`

---

**NOTE**

Since the outflow logger looks at the calling module to route the logs, you tasks must be imported using the namespace convention and not with an absolute import:

```python
#inside a commands.py file
#do this
from my_project.my_plugin import MyTask
#not this
from plugins.my_plugin.my_project.my_plugin.tasks import MyTask
```

---

## Configuring your logger

The syntax of the logging section of the config.yml file is the same as a DictConfig from the logging module. In it, you can configure the level, handlers etc.. of the logs. (see [python documentation about logging configuration](https://docs.python.org/3/library/logging.config.html#logging-config-dictschema`) for more details)

```yaml
logging:
  loggers:
    matplotlib:
      level: WARNING
      handlers: ["console"]
    my_plugin:
      level: DEBUG
      handlers: ["console"]
    my_another_plugin:
      level: ERROR
      handlers: ["console"]
```

### Log to a file

To log to a file, you can use the handler "file" in your configuration :

```yaml
logging:
  loggers:
    "": # root logger, will log everything to a file
      level: DEBUG
      handlers: ["file"]
```
