# Example workflows

##

```
              ╭−> C −−╮
              |       |
    A −−> B −−┤       ├−−> E
              |       |
              ╰−> D −−╯
```

```python
    from outflow import Command
    from mypipeline.tasks import A, B, C, D, E

    class MyCommand(Command):
        def setup_tasks():
            a = A()
            b = B()
            e = E()

            a | b | C() | e
            b | D() | e
```

Note that C and D will not be executed in parallel. This needs some work on the backend and will be possible in a future version of outflow.
