# How to install Outflow

## Install Python

### System requirements

- Python 3.7 and up
- Any linux distribution or Windows

It is recommended to have a virtual environment per pipeline. For example :

```bash
$ python3 -m venv ~/.virtualenvs/my_pipeline
$ source ~/.virtualenvs/my_pipeline/bin/activate
```

## Install the Outflow code

Outflow is release on PyPi, so inside your virtual environment, you can just use:

```bash
(my_pipeline) $ pip install outflow
```
