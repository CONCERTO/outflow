# Migrations

With [Alembic](https://alembic.sqlalchemy.org/en/latest/) and using [SQLAlchemy](https://www.sqlalchemy.org/) as the underlying engine, Outflow provides a way of propagating changes you make to your models (adding a field, deleting a model, etc.) into your relational database.

## The Commands

There are several commands which you will use to interact with migrations and Outflow's handling of database schema:

- `management db upgrade --database <DB-LABEL>`, which is responsible for upgrading to a later migration version.
- `management db downgrade --database <DB-LABEL>`, which is responsible for reverting to a previous migration version.
- `management db make_migrations`, which is responsible for creating new migrations based on the changes you have made to your models.

You should think of migrations as a version control system for your database schema. `make_migrations` is responsible for packaging up your model changes into individual migration files - analogous to commits - and `upgrade`/`upgrade` is responsible for applying/reverting those to your database.

This will add a `versions` folder to the `models` directory of your plugin. The contents of this folder need to be added to version control along with your other source files.

**WARNING**: The migration script needs to be reviewed and edited, as Alembic currently does not detect every change you make to your models. In particular, Alembic is currently unable to detect table name changes, column name changes, or anonymously named constraints. A detailed summary of limitations can be found in the [Alembic autogenerate documentation](https://alembic.sqlalchemy.org/en/latest/autogenerate.html#what-does-autogenerate-detect-and-what-does-it-not-detect). Once finalized, the migration script also needs to be added to version control.

## Backend Support

Migrations are supported on all backends that Outflow ships with :

- PostgreSQL
- SQLite
