***************
Quick reference
***************

Turn a function into a outflow task
===================================

Let's say you have this function :

.. code-block:: python

    def do_stuff():
        # some computation


To turn this function into a outflow task, you only need to add the ``@as_task`` decorator :

.. code-block:: python

    @as_task
    def DoStuff():
        # some computation

The decorator ``@as_task`` turns a function into a class so it may be better to use the class naming convention here


Specifying the outputs of a task
================================

Tasks usually return some data for the next task. You have two choices to define the outputs of your tasks :

Recommended: return annotation
******************************

.. code-block:: python

    @Target.output("returned_data")
    @as_task
    def DoStuff() -> {"returned_data": int}:
        ret = #some computation
        return {
            "returned_data": ret
        }

Alternative way: Target.output decorator
****************************************

.. code-block:: python

    @Target.output("returned_data", type=int)
    @as_task
    def DoStuff():
        ret = #some computation
        return {
            "returned_data": ret
        }
