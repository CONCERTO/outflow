########
Overview
########

************
Introduction
************

What is outflow
=================

Outflow is a framework that helps you write and run pipelines.

Outflow is a reimplementation of the `core of Poppy <https://gitlab.obspm.fr/POPPY/POPPyCore>`_, the pipeline framework
originally developed by the `RPW Operation Center <https://rpw.lesia.obspm.fr/>`_ at LESIA for their data reduction pipeline.

The objective of outflow is to add task and workflow parallelization capabilities to the Poppy framework. With outflow, you can parallelize your workflow
either on a single machine, or on a computing cluster managed by Slurm.


Installation
============

Outflow is on PyPI! You can easily install the latest stable version with :code:`pip install outflow`. Since Outflow is still in development so you might want to install
the development versions, clone and install our `git repository <https://gitlab.com/outflow-project/outflow>`_ with :code:`pip install -e .`

System requirements
-------------------

* Python 3.7 and up
* Any linux distribution or Windows
