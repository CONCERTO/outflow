# How to contribute to Outflow

Thank you for considering contributing to Outflow!

## Support questions

Please, don't use the issue tracker for this. The issue tracker is a tool to address bugs and feature requests in Outflow itself. Use one of the following resources for questions about using Outflow or issues with your own code:

- The `#get-help` channel on our Discord chat: <discord-link>
- The mailing list <mailing-list> for long term discussion or larger issues.
- Ask on [Stack Overflow](https://stackoverflow.com/questions/tagged/outflow?sort=linked). Search with Google first using: `site:stackoverflow.com python outflow {search term, exception message, etc.}`

## Reporting issues

Include the following information in your post:

- Describe what you expected to happen.
- If possible, include a [minimal reproducible example](https://stackoverflow.com/help/minimal-reproducible-example) to help us identify the issue. This also helps check that the issue is not with your own code.
- Describe what actually happened. Include the full traceback if there was an exception.
- List your Python and Outflow versions. If possible, check if this issue is already fixed in the latest releases or the latest code in the repository.

## Submitting patches

If there is not an open issue for what you want to submit, prefer opening one for discussion before working on a PR. You can work on any issue that doesn't have an open PR linked to it or a maintainer assigned to it. These show up in the sidebar. No need to ask if you can work on an issue that interests you.

Include the following in your patch:

- Use [Black](https://black.readthedocs.io) to format your code. This and other tools will run automatically if you install [pre-commit](https://pre-commit.com) using the instructions below.
- Include tests if your patch adds or changes code. Make sure the test fails without your patch.
- Update any relevant docs pages and docstrings. Docs pages and docstrings should be wrapped at 72 characters.
- Add an entry in `CHANGES.rst`. Use the same style as other entries. Also include `.. versionchanged::` inline changelogs in relevant docstrings.

### Running the tests

Run the basic test suite with pytest.

```
$ pytest
```

This runs the tests for the current environment, which is usually sufficient. CI will run the full suite when you submit your pull request. You can run the full test suite with tox if you don't want to wait.

```
$ tox
```

### Running test coverage

Generating a report of lines that do not have test coverage can indicate where to start contributing. Run `pytest` using `coverage` and generate a report.

```
$ pip install coverage
$ coverage run -m pytest
$ coverage html
```

Open `htmlcov/index.html` in your browser to explore the report.

Read more about [coverage](https://coverage.readthedocs.io).

### Building the docs

Build the docs in the `docs` directory using Sphinx.

```
$ cd docs
$ make html
```

Open `_build/html/index.html` in your browser to view the docs.

Read more about [Sphinx](https://www.sphinx-doc.org/en/stable/).
