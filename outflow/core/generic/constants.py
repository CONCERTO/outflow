# -*- coding: utf-8 -*-
#: Byte values represented in SI units
SI_STEPS = {
    "B": 1,
    "kB": 1000,
    "MB": 1000000,
    "GB": 1000000000,
    "TB": 1000000000000,
    "PB": 1000000000000000,
    "EB": 1000000000000000000,
    "ZB": 1000000000000000000000,
    "YB": 1000000000000000000000000,
}

#: Byte values represented in NIST units
NIST_STEPS = {
    "KiB": 1024,
    "MiB": 1048576,
    "GiB": 1073741824,
    "TiB": 1099511627776,
    "PiB": 1125899906842624,
    "EiB": 1152921504606846976,
}
