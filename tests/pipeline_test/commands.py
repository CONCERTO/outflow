# -*- coding: utf-8 -*-
from outflow.core.commands import RootCommand, Command


@RootCommand.subcommand()
class TestCommand(Command):
    def setup_tasks(self):
        GenData() >> PrintData()
        Dummy(identifier=1)
