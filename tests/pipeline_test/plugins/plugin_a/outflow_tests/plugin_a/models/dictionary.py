#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from outflow.core.db import Model
from sqlalchemy import String, UniqueConstraint, Column
from sqlalchemy.dialects.postgresql import INTEGER

__all__ = ["Dictionary"]


class Dictionary(Model):
    """
    The "dictionary" table contains the dictionary
    """

    id_dictionary = Column(INTEGER(), primary_key=True, nullable=False)
    identifier = Column(INTEGER(), unique=True, nullable=False)
    word = Column(
        String(16), unique=True, comment="Must be an english word.", nullable=False
    )

    __table_args__ = (UniqueConstraint("identifier", "word"),)
