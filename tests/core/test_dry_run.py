# -*- coding: utf-8 -*-

from outflow.core.commands import Command, RootCommand
from outflow.core.pipeline import config, context
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase


@as_task
def DryRunTask(dry_run=None):
    assert context.dry_run is dry_run


@RootCommand.subcommand(db_untracked=True)
class DryRunAssertTrue(Command):
    def setup_tasks(self):
        return DryRunTask(dry_run=True)


@RootCommand.subcommand(db_untracked=True)
class DryRunAssertFalse(Command):
    def setup_tasks(self):
        return DryRunTask(dry_run=False)


class TestDryRunCommand(CommandTestCase):
    def test_dry_run_command__force_dry_run(self):
        arg_list = ["dry_run_assert_true"]

        self.run_command(arg_list, force_dry_run=True)

    def test_dry_run_command__force_dry_run__dry_run_arg(self):
        arg_list = ["--dry-run", "dry_run_assert_true"]

        self.run_command(arg_list, force_dry_run=True)

    def test_dry_run_command(self):
        arg_list = ["dry_run_assert_false"]
        config.update(
            {"databases": {"default": {"dialect": "sqlite", "path": ":memory:"}}}
        )

        self.run_command(arg_list, force_dry_run=False)

    def test_dry_run_command__dry_run_arg(self):
        arg_list = ["--dry-run", "dry_run_assert_true"]

        self.run_command(arg_list, force_dry_run=False)
