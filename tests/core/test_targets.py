# -*- coding: utf-8 -*-
from typing import List
from typing_extensions import TypedDict

import pytest
from outflow.core.commands import Command
from outflow.core.exceptions import IOCheckerError
from outflow.core.target import Target, TargetException
from outflow.core.tasks import Task, as_task
from outflow.core.test import CommandTestCase, TaskTestCase


@as_task
def Return2KeysAuto():
    return {"key1": None, "key2": None}


@as_task
def ReturnOneObj() -> {"obj": int}:  # noqa : F821
    return 1


@as_task
def Input1keys(key1):
    return {}


@as_task
def Input2keys(key1, key2):
    return {}


class ReturnOneObjCommand(Command):
    def setup_tasks(self) -> List[Task]:
        return ReturnOneObj()


class AutoTargetsCommand(Command):
    def setup_tasks(self) -> List[Task]:
        return_2_keys_auto = Return2KeysAuto()
        input_2_keys = Input2keys()

        return_2_keys_auto >> input_2_keys

        return input_2_keys


class AutoTargetsFailCommand(Command):
    def setup_tasks(self):
        ReturnOneObj() >> Input2keys()


class TestOutflowTask(CommandTestCase):
    def test_target_auto_fail(self):
        self.root_command_class = AutoTargetsFailCommand
        with pytest.raises(IOCheckerError):
            self.run_command()

    def test_target_auto(self):
        self.root_command_class = AutoTargetsCommand
        self.run_command()

    def test_ret_one_obj(self):
        self.root_command_class = ReturnOneObjCommand
        self.run_command()


class TestTaskTargets(TaskTestCase):
    def test_return_wrong_type(self):
        @as_task
        def ReturnWrongTypes() -> {"output": int}:
            return {"output": "hello"}

        self.task = ReturnWrongTypes()
        with pytest.raises(TypeError):
            self.run_task()

    def test_return_wrong_type_typeddict(self):
        class TaskOutputs(TypedDict):
            output: int

        @as_task
        def ReturnWrongTypes() -> TaskOutputs:
            out: TaskOutputs = {"output": "hello"}
            return out

        self.task = ReturnWrongTypes()
        with pytest.raises(TypeError):
            self.run_task()

    def test_return_right_type_typeddict(self):
        output_value = {"output": "hello"}

        class TaskOutputs(TypedDict):
            output: str

        @as_task
        def ReturnWrongTypes() -> TaskOutputs:
            out: TaskOutputs = output_value
            return out

        self.task = ReturnWrongTypes()
        result = self.run_task()

        assert result == output_value

    def test_output_not_dict(self):
        class MySomething:
            hy: int

            def items(self, something: int, other: str):
                pass

        with pytest.raises(TargetException):

            @as_task
            def ReturnWrongTypes() -> MySomething:
                return MySomething()

            self.task = ReturnWrongTypes()

            self.run_task()

    def test_target_decorator_override_automatic(self):
        @Target.input("some_input", type=int)
        @as_task
        def SomeTask(some_input: str):
            pass

        assert SomeTask().inputs["some_input"].type == int

    def test_target_decorator_only(self):
        @Target.input("some_input", type=int)
        @as_task
        def SomeTask(some_input):
            pass

        assert SomeTask().inputs["some_input"].type == int
