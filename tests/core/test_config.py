# -*- coding: utf-8 -*-
from outflow.core.pipeline import Config


class TestContext:
    def test_sanitize_config(self):
        config = {
            "secrets": {
                "logins1": {"login": "toto", "password": "1234"},
                "logins2": "mylogin:mypassword",
            }
        }

        result = Config.sanitize(config)

        expected = {
            "logins1": {"login": "******", "password": "******"},
            "logins2": "******",
        }
        assert result["secrets"] == expected
