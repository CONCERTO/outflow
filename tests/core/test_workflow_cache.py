# -*- coding: utf-8 -*-
from .test_products import CommandTestCaseRandomTempdir
from unittest import mock

from outflow.core.product import Product, HeavyProduct
from outflow.core.commands import Command
from outflow.core.workflow import WorkflowCache, Workflow
from outflow.core.tasks import Task, as_task
from outflow.library.workflows import MapWorkflow
from outflow.core.pipeline import context, settings
from outflow.core.types import IterateOn
from pathlib import Path


class L1Product(HeavyProduct):
    pass


class L2Product(Product):
    pass


@as_task
def ReshapeKeyword(keyword: str = "reshaped") -> {"keyword": str}:
    return keyword


@as_task
def ListL0() -> {"list_l0": list}:
    """
    List L0 files
    """
    return [f"file_{i}.heavy.txt" for i in range(context.args.l0_number)]


@as_task
def ReadL0(l0_filepath: IterateOn("list_l0", str)) -> {"content": str}:
    """
    Read L0 data
    """
    return f"{l0_filepath} content"


@as_task
def ReshapeData(content: str, keyword: str) -> {"reshaped_data": str}:
    """
    Reshape data
    """
    return content + " " + keyword


@as_task
def ApplyTransferFunction(reshaped_data: str) -> {"engineering_data": str}:
    """
    Apply transfer function
    """
    return reshaped_data + " engineering"


@as_task(with_self=True)
def WriteL1(self: Task, engineering_data: str) -> {"l1_product": L1Product}:
    """
    Write L1 data
    """

    def get_filepath(data):
        import re

        filename = re.search(r"^(file_\d+)\.", data).group(1)
        return settings.TEMP_DIR / Path(f"{filename}.l1_product.txt")

    l1_product = L1Product(
        engineering_data,
        filepath=get_filepath(engineering_data),
        workflow_hash=self.parent_workflow.hash,
    )
    l1_product.write()
    return {"l1_product": l1_product}


@as_task
def ReadL1(l1_product: L1Product) -> {"l1_data": str}:
    """
    Read L1 data
    """
    return l1_product.read()


@as_task
def ApplyCalibration(l1_data: str) -> {"calibrated_data": str}:
    """
    Apply calibration
    """

    return l1_data + " calibrated"


@as_task
def RemoveNoise(calibrated_data: str) -> {"cleaned_data": str}:
    """
    Remove noise
    """
    return calibrated_data + " cleaned"


def l0_to_l1_workflow(external_task):
    """
    L0 to L1 workflow
    """

    worflow_name = (
        context.args.l0_to_l1_workflow if context.args.l0_to_l1_workflow else "l0_to_l1"
    )

    cache_dir = settings.TEMP_DIR / "l1"

    with Workflow(name=worflow_name, cache=True, cache_dir=cache_dir) as l0_to_l1:
        reshape_data = ReshapeData()
        ReadL0() >> reshape_data >> ApplyTransferFunction() >> WriteL1()
        external_task >> reshape_data

    return l0_to_l1


def l1_to_l2_workflow():
    """
    L1 to L2 workflow
    """
    with Workflow(
        name="l1_to_l2", cache=True, cache_dir=settings.TEMP_DIR / "l2"
    ) as l1_to_l2:
        ReadL1() >> ApplyCalibration() >> RemoveNoise()
    return l1_to_l2


class L0ToL2(Command):
    """
    L0 to L2 command running successively the L0 to L1 and the L1 to L2 workflows

    list_l0 -> l0_to_l2[
        l0_to_l1_workflow[
            read_l0() -> reshape_data() -> apply_transfer_function() -> write_l1()
        ] -> l1_to_l2_workflow[
            read_l1() -> apply_calibration() -> remove_noise()
        ]
    ]
    """

    reshaped_keyword = "reshaped"
    cache_map = False

    def setup_tasks(self):
        list_l0 = ListL0()
        reshape_keyword = ReshapeKeyword(keyword=self.reshaped_keyword)

        with MapWorkflow(raise_exceptions=True, cache=self.cache_map) as l0_to_l2:
            l0_to_l1_workflow(external_task=reshape_keyword) >> l1_to_l2_workflow()

        list_l0 >> l0_to_l2

        return l0_to_l2

    def add_arguments(self):
        self.add_argument("--l0_number", type=int, help="Number of L0")
        self.add_argument(
            "--l0_to_l1_workflow", type=str, help="The name of the L0 to L1 workflow"
        )


class TestDemoPipeline(CommandTestCaseRandomTempdir):
    expected_result = [
        {
            "cleaned_data": [
                "file_0.heavy.txt content reshaped engineering calibrated " "cleaned",
                "file_1.heavy.txt content reshaped engineering calibrated " "cleaned",
                "file_2.heavy.txt content reshaped engineering calibrated " "cleaned",
            ]
        }
    ]

    arg_list = ["--l0_number", "3"]

    mock_cache_write = mock.patch.object(
        WorkflowCache, "write", side_effect=WorkflowCache.write, autospec=True
    )
    mock_cache_read = mock.patch.object(
        WorkflowCache, "read", side_effect=WorkflowCache.read, autospec=True
    )

    def setup_method(self):
        self.cache_write = self.mock_cache_write.start()
        self.cache_read = self.mock_cache_read.start()
        super().setup_method()

    def teardown_method(self):
        self.mock_cache_write.stop()
        self.mock_cache_read.stop()
        super().teardown_method()

    def assert_cache_call_count(self, count_write: int, count_read: int):
        """Helper to count the number of calls to the WorkflowCache mock methods"""
        assert self.cache_write.call_count == count_write
        assert self.cache_read.call_count == count_read
        self.cache_write.call_count = 0
        self.cache_read.call_count = 0

    def test_workflow_cache(self):
        self.root_command_class = L0ToL2

        # first run, nothing in cache
        error_code, result = self.run_command(self.arg_list)

        self.assert_cache_call_count(6, 0)
        assert result == self.expected_result

        # second run, should read from cache
        error_code, result = self.run_command(self.arg_list)

        self.assert_cache_call_count(0, 6)
        assert result == self.expected_result

    def test_workflow_cache__with_modified_products(self):
        """
        Modify first workflow, second workflow should be reran too,
        even thought product L1Product did not change filepath
        """

        self.root_command_class = L0ToL2

        # compute l1 and l2 products without the modified workflow
        error_code, result = self.run_command(self.arg_list)
        self.assert_cache_call_count(6, 0)

        # run the modified workflow
        error_code, result = self.run_command(
            [*self.arg_list, "--l0_to_l1_workflow", "l0_to_l1_modified"]
        )

        # and check that product cache is invalid/not used
        self.assert_cache_call_count(6, 0)
        assert result == self.expected_result

        # and run the modified workflow again to check that product cache is used
        error_code, result = self.run_command(self.arg_list)

        self.assert_cache_call_count(0, 6)
        assert result == self.expected_result

    def test_workflow_cache__with_deleted_cache_file(self):

        self.root_command_class = L0ToL2

        # compute l1 and l2 products without the modified workflow
        error_code, result = self.run_command(self.arg_list)
        self.assert_cache_call_count(6, 0)

        # delete one of the L1 products
        list(Path(settings.TEMP_DIR / "l1").glob("*"))[0].unlink()

        error_code, result = self.run_command(self.arg_list)

        self.assert_cache_call_count(1, 5)
        assert result == self.expected_result

        # delete one of the L2 products
        l2_file_list = list(Path(settings.TEMP_DIR / "l2").glob("*"))
        # check that there is only 3 L2 product
        assert len(l2_file_list) == 3

        l2_file_list[0].unlink()

        l2_file_list = list(Path(settings.TEMP_DIR / "l2").glob("*"))
        # check that there remain only 2 L2 product
        assert len(l2_file_list) == 2

        error_code, result = self.run_command(self.arg_list)

        self.assert_cache_call_count(1, 5)
        assert result == self.expected_result

    def test_workflow_cache__change_external_edge_value(self):
        self.root_command_class = L0ToL2

        # compute l1 and l2 products
        error_code, result = self.run_command(self.arg_list)

        assert result == self.expected_result

        self.root_command_class.reshaped_keyword = "reshaped_override"

        # run the modified workflow
        error_code, result = self.run_command(self.arg_list)

        # and check that product cache is invalid/not used
        local_expected_result = [
            {
                "cleaned_data": [
                    "file_0.heavy.txt content reshaped_override engineering "
                    "calibrated cleaned",
                    "file_1.heavy.txt content reshaped_override engineering "
                    "calibrated cleaned",
                    "file_2.heavy.txt content reshaped_override engineering "
                    "calibrated cleaned",
                ]
            }
        ]

        assert result == local_expected_result

    def test_map_workflow_cache(self):
        @as_task
        def GenData(list_len) -> {"some_data": list}:
            some_data = list(range(list_len))
            return {"some_data": some_data}

        @as_task
        def Compute(some_data):
            # Simulate a big computation
            result = some_data * 2

            # return the result for the next task
            return {"computation_result": result}

        @MapWorkflow.as_workflow(cache=True, flatten_output=True)
        def compute_workflow():
            Compute()

        class ComputeData(Command):
            list_len = 3

            def setup_tasks(self):
                # setup the main workflow by using the workflow defined outside command
                compute = compute_workflow()

                GenData(list_len=self.list_len) | compute

                return compute

        self.root_command_class = ComputeData
        self.assert_cache_call_count(0, 0)

        code, result = self.run_command()
        assert result == [{"computation_result": [0, 2, 4]}]
        self.assert_cache_call_count(3, 0)

        code, result = self.run_command()
        self.assert_cache_call_count(0, 3)
        assert result == [{"computation_result": [0, 2, 4]}]

        # return a list with one more item
        self.root_command_class.list_len = 4
        code, result = self.run_command()
        self.assert_cache_call_count(1, 3)
        assert result == [{"computation_result": [6, 0, 2, 4]}]
