# -*- coding: utf-8 -*-
import re


def test_version():
    from outflow import __version__

    pattern = re.compile(r"^\d+\.\d+\.\d+(?:\.dev\d+)*$")
    assert pattern.match(__version__)
