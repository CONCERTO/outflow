# -*- coding: utf-8 -*-
import pytest
from outflow.core.generic.string import (
    MemoryUnitException,
    MemoryValueException,
    as_byte,
    to_camel_case,
    to_snake_case,
)


def test_to_camel_case():
    assert to_camel_case("test_my_case") == "TestMyCase"
    assert to_camel_case("test_num1") == "TestNum1"
    assert to_camel_case("test_num_1") == "TestNum1"


def test_to_snake_case():
    assert to_snake_case("TestMyCase") == "test_my_case"
    assert to_snake_case("testMyCase") == "test_my_case"
    assert to_snake_case("testAAA") == "test_aaa"
    assert to_snake_case("testA1WithNums2") == "test_a1_with_nums2"


def test_as_byte():
    assert as_byte(" 11 MiB") == 11.534336e6
    assert as_byte(" 1e3 MB ") == 1e9
    assert as_byte("1e3MB") == 1e9
    assert as_byte("4B") == 4

    with pytest.raises(MemoryValueException):
        as_byte("1e3M")

    with pytest.raises(MemoryUnitException):
        assert as_byte("1e3YiB") == 1e9
