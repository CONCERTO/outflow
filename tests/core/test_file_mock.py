# -*- coding: utf-8 -*-
from outflow.core.test.file_mock import create_file_mock, patch_open


def test_file_mock():
    def write_to(result):
        def _inner(s):
            result.append(s)

        return _inner

    a_result = ["a__"]

    a_mock = create_file_mock(
        read_data="a mock - content", write_callback=write_to(a_result)
    )

    b_result = []

    b_mock = create_file_mock(
        read_data="b mock - content", write_callback=write_to(b_result)
    )
    mocked_files = {
        "a": a_mock,
        "b": b_mock,
    }
    with patch_open(mocked_files):
        with open("a") as file_handle:
            assert "a mock - content" == file_handle.read()  # prints a mock - content
            file_handle.write("aa")
            file_handle.write("1")
            assert "*".join(a_result) == "a__*aa*1"

        with open("b") as file_handle:
            assert (
                "b mock - content" == file_handle.read()
            )  # prints b mock - different content
            file_handle.write("bb")
            file_handle.write("2")
            assert "*".join(b_result) == "bb*2"
