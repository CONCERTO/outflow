# -*- coding: utf-8 -*-
# import pytest
import logging

import pytest
from outflow.core.commands import Command
from outflow.core.exceptions import IOCheckerError
from outflow.core.target import Target
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase
from outflow.core.workflow import Workflow

logger = logging.getLogger(__name__)


@Target.output("b", type=int)
@as_task
def ReturnBInt():
    return {"b": 1}


@Target.output("a", type=int)
@as_task
def ReturnAInt():
    return {"a": 1}


@Target.output("a", type=str)
@as_task
def ReturnAStr():
    return {"a": "test"}


@as_task
def InputAInt(a: int):
    pass


class PassTypecheckCommand(Command):
    def setup_tasks(self):
        return_a_int = ReturnAInt()
        input_a_int = InputAInt()

        return_a_int >> input_a_int
        return input_a_int


class FailTypecheckTypeCommand(Command):
    def setup_tasks(self):
        return_a_str = ReturnAStr()
        input_a_int = InputAInt()

        return_a_str >> input_a_int
        return input_a_int


class FailTypecheckTargetNameCommand(Command):
    def setup_tasks(self):
        return_b_int = ReturnBInt()
        input_a_int = InputAInt()

        return_b_int >> input_a_int
        return input_a_int


class FailTypecheckWorkflow(Command):
    def setup_tasks(self):
        with Workflow():
            ReturnBInt() >> InputAInt()


class TestStaticTypechecker(CommandTestCase):
    def test_pass_typecheck(self):

        self.root_command_class = PassTypecheckCommand
        self.run_command([])

    def test_fail_typecheck_bad_target_name(self):

        self.root_command_class = FailTypecheckTargetNameCommand
        with pytest.raises(IOCheckerError):
            self.run_command([])

    def test_fail_typecheck_bad_type(self):

        self.root_command_class = FailTypecheckTypeCommand
        with pytest.raises(TypeError):
            self.run_command([])

    def test_workflow_iochecker_fail(self):

        self.root_command_class = FailTypecheckWorkflow

        with pytest.raises(IOCheckerError):
            self.run_command()
