# -*- coding: utf-8 -*-
import pathlib
import subprocess
import sys

from outflow.core.test import CommandTestCase
from unittest import mock


class TestOutflowManagement(CommandTestCase):
    def test_create_pipeline(self):
        import tempfile

        with tempfile.TemporaryDirectory() as dir_path:
            path_obj = pathlib.Path(dir_path)
            arg_list = [
                "management",
                "create",
                "pipeline",
                "my_pipeline",
                "--pipeline_dir",
                dir_path,
            ]
            self.run_command(arg_list)
            pipeline_files = set(
                path.relative_to(path_obj).as_posix() for path in path_obj.glob("*")
            )

            assert pipeline_files == {
                "config.yml",
                "manage.py",
                "requirements.txt",
                "settings.py",
                "plugins",
            }

            arg_list = [
                "management",
                "create",
                "plugin",
                "my_plugin",
                "--namespace",
                "my_namespace",
                "--plugin_dir",
                str(path_obj / "plugins" / "my_plugin"),
            ]
            self.run_command(arg_list)
            plugin_files = set(
                path.relative_to(path_obj).as_posix()
                for path in path_obj.glob("plugins/**/*")
            )

            import fileinput

            for line in fileinput.input(path_obj / "settings.py", inplace=True):
                if "your plugins go here" in line:
                    print("\t'my_namespace.my_plugin',")
                print(line, end="")

            assert plugin_files == {
                "plugins/my_plugin",
                "plugins/my_plugin/requirements.txt",
                "plugins/my_plugin/README.rst",
                "plugins/my_plugin/setup.py",
                "plugins/my_plugin/MANIFEST.in",
                "plugins/my_plugin/my_namespace",
                "plugins/my_plugin/my_namespace/__init__.py",
                "plugins/my_plugin/my_namespace/my_plugin",
                "plugins/my_plugin/my_namespace/my_plugin/__init__.py",
                "plugins/my_plugin/my_namespace/my_plugin/commands.py",
                "plugins/my_plugin/my_namespace/my_plugin/models",
                "plugins/my_plugin/my_namespace/my_plugin/models/model.py",
                "plugins/my_plugin/my_namespace/my_plugin/models/__init__.py",
                "plugins/my_plugin/my_namespace/my_plugin/models/versions",
                "plugins/my_plugin/my_namespace/my_plugin/models/versions/README.rst",
                "plugins/my_plugin/my_namespace/my_plugin/tasks.py",
                "plugins/my_plugin/my_namespace/my_plugin/tests.py",
            }

            out = subprocess.run(
                [sys.executable, path_obj / "manage.py", "my_plugin"],
                check=True,
                text=True,
                capture_output=True,
            ).stdout

            out = " ".join(out.split())

            assert "Hello from my_plugin" in out

            proc = subprocess.run(
                [sys.executable, path_obj / "manage.py", "compute_data"],
                check=True,
                text=True,
                capture_output=True,
            )

            out = " ".join(proc.stdout.split())

            assert "Result of the computation: 42" in out

    def test_make_migrations(self):
        pass

    def test_shell(self):
        def start_ipython_strip_kwargs(*args, **kwargs):
            from IPython.testing.globalipapp import start_ipython

            return start_ipython()

        with mock.patch("IPython.start_ipython", start_ipython_strip_kwargs):
            self.run_command(["management", "shell"])

    def test_debug_subcommand(self):
        self.run_command(["management", "debug", "1"])

    def test_list_runs_subcommand(self):
        self.run_command(["management", "list_runs"])
