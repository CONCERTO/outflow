# -*- coding: utf-8 -*-
# import pytest
# from outflow.core.commands import Command
# from outflow.core.pipeline import config
# from outflow.core.tasks import Task
# from outflow.core.test import CommandTestCase
# from outflow.ray.tasks import ParallelTask
#
#
# @as_task
# def A() -> {"data_A": str}:
#     return {"data_A": "A"}
#
#
# @ParallelTask.as_task()
# def B(data_A: str) -> {"data_B": str}:
#     return {"data_B": data_A + "B"}
#
#
# @ParallelTask.as_task()
# def C(data_A: str) -> {"data_C": str}:
#     return {"data_C": data_A + "C"}
#
#
# as_task
# def D(data_B: str, data_C: str) -> {"data_D": str}:
#     result = data_B + data_C + "D"
#
#     return {"data_D": result}
#
#
# class MyCommand(Command):
#     """"""
#
#     def setup_tasks(self):
#         a = A()
#         b = B()
#         c = C()
#         d = D()
#
#         a >> b >> d
#         a >> c >> d
#
#         return d
#
#     # assert return_value["data_D"] == "ABACD"
#

# @pytest.mark.ray_backend
# class TestOutflowCommand(CommandTestCase):
#     def test_command(self):
#
#         self.root_command_class = MyCommand
#
#         config["backend"] = "ray"
#
#         error_code, result = self.run_command()
#
#         assert result == [{"data_D": "ABACD"}]
