# -*- coding: utf-8 -*-
import logging

import pytest
from outflow.core.commands import Command
from outflow.core.target import Target
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase

logger = logging.getLogger(__name__)


def write_to(result):
    """A mock function to replace the standard 'write' method

    The result of each 'write' call will be stored in the 'result' list

    Args:
        result (list): A list where the content of the 'write' calls are stored
    """

    def _inner(s):
        result.append(s)

    return _inner


@as_task
def NoTargetReturnObjectTask():
    return {"something": "obj"}


@as_task
def InTargetSomethingTask(something: str):
    pass


@Target.output("small_array")
@as_task
def GenArray():
    return {"small_array": [1, 2, 3]}


@Target.output("result")
@as_task
def ComputeSum(small_array):
    result = sum(small_array)
    return {"result": result}


@Target.output("result")
@as_task
def SubcommandTask():
    result = "write in sub task"

    return {"result": result}


class MyCommand(Command):
    """
    Test the outflow command
    """

    def setup_tasks(self):
        gen_array = GenArray()
        compute_sum_task = ComputeSum()

        gen_array >> compute_sum_task
        return compute_sum_task

    def add_arguments(self):
        self.add_argument("--input_1", type=int, help="input 1")


class CommandFail(Command):
    """
    Test the outflow sub command
    """

    def setup_tasks(self):
        return_something_task = NoTargetReturnObjectTask()
        in_target_something_task = InTargetSomethingTask()
        return_something_task >> in_target_something_task
        return in_target_something_task


@MyCommand.subcommand()
class MySubCommand(Command):
    """
    Test the outflow sub command
    """

    def setup_tasks(self):
        subcommand_task = SubcommandTask()
        # pipeline_args() >> subcommand_task
        return subcommand_task

    def add_arguments(self):
        self.add_argument("sub_input", type=str, help="sub input")


class TestOutflowCommand(CommandTestCase):
    def test_command(self):
        self.root_command_class = MyCommand
        arg_list = []

        error_code, result = self.run_command(arg_list)

        assert result == [{"result": 6}]

    def test_sub_command(self):
        self.root_command_class = MyCommand
        arg_list = ["my_sub_command", "a_string"]

        error_code, result = self.run_command(arg_list)

        assert result == [{"result": "write in sub task"}]

    def test_sub_command_with_system_exit(self):
        arg_list = ["my_sub_command", "--help"]

        self.root_command_class = MyCommand

        with pytest.raises(SystemExit):
            self.run_command(arg_list)

    def test_pipe_auto_targets(self):
        self.root_command_class = CommandFail

        self.run_command([])

    def test_pipe_or_operator(self):
        @as_task
        def Something(result: int):
            pass

        class MyCommand(Command):
            def setup_tasks(self):
                GenArray() | ComputeSum() | Something()

        self.root_command_class = MyCommand

        self.run_command([])
