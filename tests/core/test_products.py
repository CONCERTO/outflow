# -*- coding: utf-8 -*-
import shutil
import tempfile
from pathlib import Path
from unittest import mock

import pytest

from outflow.core.workflow import WorkflowCache, Workflow
from outflow.core.commands import Command
from outflow.core.pipeline import settings, config
from outflow.core.target import Target
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase


class CommandTestCaseRandomTempdir(CommandTestCase):
    @pytest.fixture(autouse=True)
    def setup_within_pipeline_context(self, with_pipeline_context_manager):
        settings.PLUGINS = self.PLUGINS
        self.temp_dir = tempfile.mkdtemp(prefix="test_outflow_")
        settings.TEMP_DIR = Path(self.temp_dir)

    def teardown_method(self):
        super().teardown_method()
        shutil.rmtree(self.temp_dir, ignore_errors=True)


@as_task
def GiveSize(size: int = 0) -> {"size": int}:
    return size


@as_task
def GenArray(size: int) -> {"small_array": list}:
    return {"small_array": list(range(size))}


@as_task
def ComputeSum(small_array) -> {"result": int}:
    return sum(small_array)


@as_task
def MultiplyArg(result, multiply_value: int = 2) -> {"result": int}:
    return result * multiply_value


@Target.parameter("multiply_value")
@as_task
def MultiplyConfig(result: int, multiply_value: int) -> {"result": int}:
    return result * multiply_value


class TestProducts(CommandTestCaseRandomTempdir):

    mock_cache_write = mock.patch.object(
        WorkflowCache, "write", side_effect=WorkflowCache.write, autospec=True
    )
    mock_cache_read = mock.patch.object(
        WorkflowCache, "read", side_effect=WorkflowCache.read, autospec=True
    )

    def assert_cache_call_count(self, count_write: int, count_read: int):
        """Helper to count the number of calls to the WorkflowCache mock methods"""
        assert self.cache_write.call_count == count_write
        assert self.cache_read.call_count == count_read
        self.cache_write.call_count = 0
        self.cache_read.call_count = 0

    def setup_method(self):
        self.cache_write = self.mock_cache_write.start()
        self.cache_read = self.mock_cache_read.start()
        super().setup_method()

    def teardown_method(self):
        self.mock_cache_write.stop()
        self.mock_cache_read.stop()
        super().teardown_method()

    def test_products_different_workflow_inputs(self):
        """Check that workflow with different inputs make different products"""

        def compute_workflow():
            with Workflow(cache=True, name="Compute") as compute:
                GenArray() >> ComputeSum()

            return compute

        def multiply_workflow():
            with Workflow(cache=True, name="Multiply") as multiply:
                MultiplyArg()

            return multiply

        class ComputeSumCommand(Command):
            def setup_tasks(self):

                multiply = multiply_workflow()

                GiveSize(size=self._size) >> compute_workflow() >> multiply

                return multiply

        self.root_command_class = ComputeSumCommand
        ComputeSumCommand._size = 4
        # Run the command
        code, result = self.run_command()

        # both workflows called for the first time : 2 writes
        self.assert_cache_call_count(2, 0)

        assert result == [{"result": 12}]

        self.run_command()

        # both workflows skipped : 2 writes
        self.assert_cache_call_count(0, 2)

        ComputeSumCommand._size = 5
        code, result = self.run_command()

        # inputs changed, both workflows run again : 2 writes
        self.assert_cache_call_count(2, 0)

        assert result == [{"result": 20}]

    def test_products_different_config(self):
        """Checks that products change when parameters of a task change"""

        def compute_workflow_f():
            with Workflow(cache=True, name="Compute") as w:
                GenArray() >> ComputeSum()

            return w

        def multiply_workflow_f():
            with Workflow(cache=True, name="Multiply") as multiply:
                MultiplyConfig()

            return multiply

        class ComputeSumCommand(Command):
            def setup_tasks(self):

                compute_workflow = compute_workflow_f()
                multiply_workflow = multiply_workflow_f()

                GiveSize(size=4) >> compute_workflow >> multiply_workflow

                return multiply_workflow

        config.update({"parameters": {"multiply_config": {"multiply_value": 2}}})

        self.root_command_class = ComputeSumCommand

        code, result = self.run_command()

        # both workflows called for the first time : 2 writes
        self.assert_cache_call_count(2, 0)

        assert result == [{"result": 12}]

        # changing config to force another hash
        config.update({"parameters": {"multiply_config": {"multiply_value": 3}}})

        code, result = self.run_command()

        # Only multiply workflow is run again
        self.assert_cache_call_count(1, 1)

        assert result == [{"result": 18}]

    def test_product_subclass(self):
        # not so useful anymore, maybe delete this test
        class UnhashableHeavyObject:
            def __init__(self, some_attribute):
                self.some_attribute = some_attribute

        @as_task
        def GenHeavyObject() -> {"heavy_object": UnhashableHeavyObject}:
            return UnhashableHeavyObject(some_attribute=42)

        @as_task
        def DoSomethingWithObject(heavy_object: UnhashableHeavyObject):
            assert heavy_object.some_attribute == 42

        def do_something_workflow():
            with Workflow(cache=True, name="DoSomething") as do_something:
                DoSomethingWithObject()

            return do_something

        class ComputeSumCommand(Command):
            def setup_tasks(self):

                with Workflow(name="GenObject") as gen_obj:
                    GenHeavyObject()

                gen_obj >> do_something_workflow()

        self.root_command_class = ComputeSumCommand

        # Run the command
        code, result = self.run_command()

        # Workflow run for the first time : 1 write
        self.assert_cache_call_count(1, 0)

        # Run the command
        code, result = self.run_command()

        self.assert_cache_call_count(0, 1)

    def test_same_name_same_input_but_different_graph(self):
        def compute_workflow_1():
            with Workflow(cache=True, name="Compute") as compute:
                GenArray(size=1) >> ComputeSum()

            return compute

        def compute_workflow_2():
            with Workflow(cache=True, name="Compute") as multiply:
                MultiplyArg(result=1, multiply_value=2)

            return multiply

        class Command1(Command):
            def setup_tasks(self):
                compute_workflow_1()

        class Command2(Command):
            def setup_tasks(self):
                compute_workflow_2()

        self.root_command_class = Command1

        self.run_command()

        # First workflow called: write
        self.assert_cache_call_count(1, 0)

        self.run_command()

        # First workflow called again: read
        self.assert_cache_call_count(0, 1)

        self.root_command_class = Command2

        self.run_command()

        # Second workflow called: write
        self.assert_cache_call_count(1, 0)

    def test_config_custom_cache_dir(self):
        def compute_workflow_1():
            with Workflow(cache=True, name="Compute") as compute:
                GenArray(size=1) >> ComputeSum()

            return compute

        class Command1(Command):
            def setup_tasks(self):
                compute_workflow_1()

        config.update(
            {"workflow_cache_dir": str(settings.TEMP_DIR / "custom_cache_dir")}
        )
        self.root_command_class = Command1

        self.run_command()

        assert (
            settings.TEMP_DIR
            / "custom_cache_dir"
            / "artifact_e761250d696028434c4b86a807e4dd2248733112b4603e1e0015d8d45329b53b"
        ).exists()
