# -*- coding: utf-8 -*-
from outflow.core.commands import Command
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase
from outflow.library.tasks import PipelineArgs


@as_task
def SomeTask(arg_from_cli: int):
    assert arg_from_cli == 5


class TestPipelineArgsCommand(Command):
    def add_arguments(self):
        self.add_argument("--arg-from-cli", type=int)

    def setup_tasks(self):
        PipelineArgs() | SomeTask()


class TestCase(CommandTestCase):
    def test_pipeline_args(self):
        self.root_command_class = TestPipelineArgsCommand

        self.run_command(["--arg-from-cli", "5"])
