# -*- coding: utf-8 -*-
from typing import List, Optional

import pytest

from outflow.core.commands import Command
from outflow.core.exceptions import IOCheckerError
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase
from outflow.core.types import IterateOn
from outflow.library.workflows import MapWorkflow, IterativeWorkflow


@as_task
def ExternalTask() -> {"external_value": str}:
    return "something"


@as_task
def DoSomethingIteratively(
    external_value: str, iter_value: Optional[int] = None
) -> {"additional_output": str, "iter_value": int}:
    assert external_value == "something"
    # if None, initialize criterion
    if iter_value is None:
        crit = 0
    else:
        crit = iter_value + 1
    return {"additional_output": "my_output", "iter_value": crit}


@as_task
def NestedIterTaskInside(
    external_value: str, crit1: Optional[int] = None, crit2: Optional[int] = None
) -> {"additional_output": str, "crit1": int, "crit2": int}:
    if crit2 is None:
        crit2 = 0
    else:
        crit2 = crit2 + 1
    if crit1 is None:
        crit1 = 0
    elif crit2 == 3:
        crit2 = 0
        crit1 = crit1 + 1
    return {"additional_output": "my_output", "crit1": crit1, "crit2": crit2}


@as_task
def NestedReturn(
    crit1: Optional[int] = None, crit2: Optional[int] = None
) -> {"crit1": Optional[int], "crit2": Optional[int]}:
    return {"crit1": crit1, "crit2": crit2}


@as_task
def ReturnInput(iter_value: Optional[int] = None) -> {"iter_value": int}:
    return iter_value


@as_task
def DoNothing():
    pass


class TestIterativeWorkflow(CommandTestCase):
    def test_iterative_break(self):
        def break_func(iter_value, **kwargs):
            if iter_value is None:
                return False
            else:
                return iter_value == 5

        class IterativeCommand(Command):
            def setup_tasks(self):

                with IterativeWorkflow(
                    max_iterations=10, break_func=break_func
                ) as iter_workflow:
                    DoNothing() >> DoSomethingIteratively(external_value="something")

                return iter_workflow

        self.root_command_class = IterativeCommand

        _, result = self.run_command([])

        assert result == [{"additional_output": "my_output", "iter_value": 5}]

    def test_iterative_max_iter(self):
        def break_func(**kwargs):
            return False

        class IterativeCommand(Command):
            def setup_tasks(self):
                external_task = ExternalTask()
                with IterativeWorkflow(
                    max_iterations=10, break_func=break_func
                ) as iter_workflow:
                    DoSomethingIteratively()

                external_task >> iter_workflow

                return iter_workflow

        self.root_command_class = IterativeCommand

        _, result = self.run_command([])

        assert result == [{"additional_output": "my_output", "iter_value": 9}]

    def test_nested_iterative_workflows_with_external_edge(self):
        def break_func(**kwargs):
            return False

        class IterativeCommand(Command):
            def setup_tasks(self):
                external_task = ExternalTask()
                with IterativeWorkflow(
                    name="it1",
                    max_iterations=3,
                    break_func=break_func,
                ) as iter_workflow1:
                    with IterativeWorkflow(
                        name="it2",
                        max_iterations=3,
                        break_func=break_func,
                    ):
                        inside = NestedIterTaskInside()
                        external_task >> inside

                return iter_workflow1

        self.root_command_class = IterativeCommand

        _, result = self.run_command([])

        assert result == [{"crit1": 2, "crit2": 2, "additional_output": "my_output"}]

    def test_iterative_with_external_edge(self):
        def break_func(**kwargs):
            return False

        class IterativeCommand(Command):
            def setup_tasks(self):
                external_task = ExternalTask()
                with IterativeWorkflow(
                    max_iterations=10, break_func=break_func
                ) as iter_workflow:
                    inside = DoSomethingIteratively()
                    external_task >> inside

                return iter_workflow

        self.root_command_class = IterativeCommand

        _, result = self.run_command([])

        assert result == [{"additional_output": "my_output", "iter_value": 9}]

    def test_mapworkflow_inside_iterative_task(self):
        def break_func(iter_value, **kwargs):
            return iter_value > 500

        @as_task
        def GenList() -> {"l": List[int]}:
            return {
                "l": [1, 2, 3],
            }

        @as_task
        def DoSomethingIterativelyInMappedWorkflow(
            some_int: IterateOn("l", int),
            iter_value: Optional[int] = None,
        ) -> {"additional_output": str, "iter_value": int}:
            assert some_int in [1, 2, 3]

            if iter_value is None:
                crit = 0
            else:
                crit = iter_value + 1

            return {"additional_output": "my_output", "iter_value": crit}

        @as_task
        def Reduce(additional_output: List, iter_value: List) -> {"iter_value": int}:
            return sum(iter_value)

        class IterativeCommand(Command):
            def setup_tasks(self):
                with IterativeWorkflow(
                    max_iterations=10, break_func=break_func
                ) as iter_workflow:
                    with MapWorkflow(raise_exceptions=True) as map_task:
                        DoSomethingIterativelyInMappedWorkflow()

                    GenList() >> map_task >> Reduce()

                return iter_workflow

        self.root_command_class = IterativeCommand

        _, result = self.run_command([])

        assert result == [{"iter_value": 1092}]

    def test_mapworkflow_inside_iterative_task_with_external_task(self):
        def break_func(iter_value, **kwargs):
            return iter_value > 500

        @as_task
        def GenList() -> {"l": List[int]}:
            return {
                "l": [1, 2, 3],
            }

        @as_task
        def DoSomethingIterativelyInMappedWorkflowWithExternalEdge(
            external_value: str,
            some_int: IterateOn("l", int),
            iter_value: Optional[int] = None,
        ) -> {"additional_output": str, "iter_value": int}:
            assert external_value == "something"
            assert some_int in [1, 2, 3]

            if iter_value is None:
                crit = 0
            else:
                crit = iter_value + 1

            return {"additional_output": "my_output", "iter_value": crit}

        @as_task
        def Reduce(iter_value: List, additional_output: List) -> {"iter_value": int}:
            # map_workflows return the list of outputs of the end tasks, ie a list of dict
            # since the map workflow only has one task, we can simply get the
            # result with map_out[0]

            return sum(iter_value)

        class IterativeCommand(Command):
            def setup_tasks(self):
                external_task = ExternalTask()
                with IterativeWorkflow(
                    max_iterations=10, break_func=break_func
                ) as iter_workflow:
                    with MapWorkflow(raise_exceptions=True) as map_task:
                        (
                            external_task
                            >> DoSomethingIterativelyInMappedWorkflowWithExternalEdge()
                        )

                    GenList() >> map_task >> Reduce()

                return iter_workflow

        self.root_command_class = IterativeCommand

        _, result = self.run_command([])

        assert result == [{"iter_value": 1092}]

    def test_wrong_input(self):
        def break_func(iter_value, **kwargs):
            if iter_value is None:
                return False
            else:
                return iter_value == 5

        class IterativeCommand(Command):
            def setup_tasks(self):

                with IterativeWorkflow(
                    max_iterations=10, break_func=break_func
                ) as iter_workflow:
                    # remove "external_value" bound kwargs to trigger error
                    DoNothing() >> DoSomethingIteratively()

                return iter_workflow

        self.root_command_class = IterativeCommand

        with pytest.raises(IOCheckerError):
            _, result = self.run_command([])
