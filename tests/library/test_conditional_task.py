# -*- coding: utf-8 -*-
import logging
from typing import Union

from outflow.core.commands import Command
from outflow.core.target import Target
from outflow.core.tasks import as_task
from outflow.core.test import CommandTestCase
from outflow.core.types import Skipped
from outflow.library.tasks import IfThenElse, MergeTask

logger = logging.getLogger(__name__)


@Target.output("out1")
@as_task
def A():
    return {"out1": "a"}


@as_task
def B(out1: str) -> {"out1": str, "out2": str}:
    return {"out1": out1, "out2": "b"}


@as_task
def C(out1: str) -> {"out1": str, "out2": str}:
    return {"out1": out1, "out2": "c"}


@as_task
def C_manually_return_skipped(
    out1: str,
) -> {"out1": Union[str, Skipped], "out2": Union[str, Skipped]}:
    return Skipped()


@as_task
def LastTask(out1: str, out2: str) -> {"last_result": str}:
    last_result = out1 + out2
    return {"last_result": last_result}


class MergeCommand(Command):
    """
    Test manual merge
    """

    def setup_tasks(self):
        a = A()
        b = B()
        c = C_manually_return_skipped()
        merge_task = MergeTask()

        a >> b
        a >> c
        b >> merge_task
        c >> merge_task
        return merge_task


class IfElseTaskCommand(Command):
    @staticmethod
    def condition(**kwargs):
        raise NotImplementedError()

    # def setup_tasks(self):
    #     a = A()
    #     b = B()
    #     c = C()
    #     then_workflow, else_workflow = IfElse(self.condition)
    #     merge_task = MergeTask()
    #     last_task = LastTask()
    #
    #     a >> then_workflow >> b >> merge_task
    #     a >> else_workflow >> c >> merge_task >> last_task
    #
    #
    #     return last_task

    def setup_tasks(self):

        a = A()
        b = B()
        c = C()

        if_t, then, else_t = IfThenElse(self.condition)

        a >> if_t
        a >> then
        a >> else_t

        then >> b
        else_t >> c

        merge_task = MergeTask()
        last_task = LastTask()

        b >> merge_task
        c >> merge_task >> last_task

        return last_task


class ThenCommand(IfElseTaskCommand):
    """
    Test the IfElse task in the "then" branch
    """

    @staticmethod
    def condition(out1, **kwargs):
        return out1 != "a"


class ElseCommand(IfElseTaskCommand):
    """
    Test the IfElse task in the "then" branch
    """

    @staticmethod
    def condition(out1, **kwargs):
        return out1 == "a"


class TestIfElse(CommandTestCase):
    def test_command_else_pass(self):
        self.root_command_class = ElseCommand
        error_code, result = self.run_command([])
        assert result == [{"last_result": "ab"}]

    def test_command_then_pass(self):
        self.root_command_class = ThenCommand
        error_code, result = self.run_command([])
        assert result == [{"last_result": "ac"}]


class TestMerge(CommandTestCase):
    def test_merge_branch(self):
        self.root_command_class = MergeCommand
        error_code, result = self.run_command([])
        assert result == [{"out1": "a", "out2": "b"}]
