# -*- coding: utf-8 -*-
import pytest
from outflow.core.generic.context_manager import ManagedClass, ReadWriteContextManager
from outflow.core.generic.context_manager.managed_class import ManagedClassException


def test_managed_class():
    class MyManager(ReadWriteContextManager):
        """Create a read-write context manager"""

        pass

    class MyManagedClass(ManagedClass, context_manager=MyManager):
        def __init__(self, *, name):
            self.name = name

    with pytest.raises(ManagedClassException):
        len(MyManagedClass)

    with MyManager() as manager_a:
        # check len method
        assert len(MyManagedClass) == 0

        print(manager_a.__dict__)

        # check data insertion
        instance_a = MyManagedClass(name="a")
        assert instance_a is MyManagedClass.manager["a"]
        assert len(MyManagedClass) == 1

        with MyManager() as manager_b:
            instance_b = MyManagedClass(name="b")
            assert len(MyManagedClass) == 2

            # check iter on class
            for instance in MyManagedClass:
                assert instance in [instance_a, instance_b]

        # Check the persistence of context content
        assert len(MyManagedClass) == 2

        # override instance_a
        instance_aa = MyManagedClass(name="a")

        assert (
            instance_a is MyManagedClass.manager["a"]
            and instance_aa is MyManagedClass.manager["a"]
        )

        assert len(MyManagedClass) == 2

        # remove instance_b
        MyManagedClass.remove(MyManagedClass.manager["b"])

        assert len(MyManagedClass) == 1

    with pytest.raises(ManagedClassException):
        len(MyManagedClass)

    # check if the context is reset on context manager exit
    assert manager_a.context is None and manager_a.parent_context is None
    assert manager_b.context is None and manager_b.parent_context is None

    with MyManager():
        assert len(MyManagedClass) == 0
