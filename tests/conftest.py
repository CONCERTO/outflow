# -*- coding: utf-8 -*-
import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--runslurm", action="store_true", default=False, help="run slurm tests"
    )


def pytest_configure(config):
    config.addinivalue_line("markers", "slurm: mark test as slurm to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslurm"):
        # --runslurm given in cli: do not skip slurm tests
        return
    skip_slurm = pytest.mark.skip(reason="need --runslurm option to run")
    for item in items:
        if "slurm" in item.keywords:
            item.add_marker(skip_slurm)
