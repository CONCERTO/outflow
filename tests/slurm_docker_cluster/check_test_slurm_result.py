# -*- coding: utf-8 -*-
import argparse
import json
import pathlib


def main(result_filepath):
    expected_result = [
        list(range(0, 3)),
        ["test_val_out"] * 3,
    ]
    with open(result_filepath) as file:
        result = json.load(file)
        assert result == expected_result, f"{result} != {expected_result}"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Check the result of the "test_slurm" command.'
    )
    parser.add_argument(
        "filepath", type=pathlib.Path, help="Filepath of the slurm command result"
    )

    args = parser.parse_args()
    main(args.filepath)
