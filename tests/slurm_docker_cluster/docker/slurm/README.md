This folder contains a copy of the Dockerfile used to create the slurm image. It should not be used nor modified.
If you want to update the slurm configuration, please use the Dockerfile and conf files in the pipeline directory
